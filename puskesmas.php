<?php 
require 'function.php';

$kode_kec =$_GET["pilihkecamatan"];
$puskesmas = query("SELECT * FROM puskesmas where kode_kecamatan = '$kode_kec'")
// function query($query) {
// 	global $conn;
// 	$result = mysqli_query($conn, $query);
// 	$rows = [];
// 	while( $row = mysqli_fetch_assoc($result) ) {
// 		$rows[] = $row;
// 	}
// 	return $rows;
// }
// session_start();

// $conn = mysqli_connect("localhost", "root", "", "registrasi_puskesmas");
// $kec = $_GET["pilihkecamatan"];

// $puskesmas = query("SELECT * FROM puskesmas where kode_kecamatan = '$kec'");


?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Memilih Puskesmas</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@600&family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="reset.css">
    <link rel="stylesheet" href="Header.css">
    <link rel="stylesheet" href="puskesmas.css">
    <link rel="stylesheet" href="footer.css">
</head>

<body>
    <header>
            <div class="atas">
                <a href="HOME.html">
                    <img src="icon/logo-puskesmas-terbaru-sesuai-permenkes-tahun-1.png" alt="foto puskesmas">
                    <h1>REKES</h1>
                </a>
            <ul>
                <li> <a href="HOME.html"> REGISTRASI </a></li>
                <li> <a href="pencarian.php" target="blank"> PENCARIAN </a></li>
                <li> <a href="petunjuk.html"> PETUNJUK </a></li>
                <li> <a href="pengunjung.php"> ANTRIAN </a></li>
                <li> <a href="maps.html" target="blank"> MAPS </a></li>
            </ul>
        </div>

    </header>

    <main>
        <div class="judul">
            <h1>PILIH PUSKESMAS</h1>
        </div> 
    <?php 
    
    // $kecamatan = array("mataram", "jawa","selong","narmada");
    foreach($puskesmas as $pus):  ?>
    
        <a href="poli.php?pilihpuskesmas=<?php echo $pus["kode_puskesmas"]?>">
            <div class="bungkusMenu">
                <div class="menu">
                    <img src="icon/rehabilitation1.png" alt="poli.php">
                </div>
                <p><?php echo strtoupper($pus["nama_puskesmas"])  ?>
                </p>
            </div>
        </a>
    <?php endforeach;  ?>
        
    </main>

    <footer>
        <div class="containerFooter">
            <div class="rekes">
                <h2>REKES</h2>
                <p>&copy;2020 UwU Dev, Inc</p>
                <br><br>
                <p>Mataram, <br>Lombok Indonesia</p>
            </div>
            <div class="tentang">
                <ul>
                    <a href="">
                        <li>HOME</li>
                    </a>
                    <li>OPEN <br> 08.00 WITA </li>
                </ul>
            </div>
            <div class="contact">
                <ul>
                    <a href="">
                        <li>CONTACT</li>
                    </a>
                    <a href="">
                        <li>Term of Use</li>
                    </a>
                    <a href="">
                        <li>Privacy Policy</li>
                    </a>
                    </a>
                </ul>
            </div>
            <div class="faq">
                <ul>
                    <a href="faq.php">
                        <li>FAQ</li>
                    </a>
                        <li> 
                            <a href="">
                                <img src="icon/youtube.png" alt="">
                            </a>
                            <a href="">
                                <img src="icon/facebook-logo.png" alt="">
                            </a>
                        </li>
                </ul>
            </div>
        </div>
    </footer>
</body>
</html>