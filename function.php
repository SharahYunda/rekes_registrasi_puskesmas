<?php 
session_start();
   $conn = mysqli_connect("localhost", "root", "", "registrasi_puskesmas");

function query($query){
    global $conn;
    $result = mysqli_query($conn, $query);
    $rows= [];
    while ($row = mysqli_fetch_assoc($result)){
        $rows[]=$row;
    }
    return $rows;
}

function registrasi($data){
    global $conn;

    $nik= $data["NIK"];
    $nama= $data["Nama"];
    $kpl_keluarga= $data["kpl_keluarga"];
    $kelamin = $data["status"];
    $tanggal_lahir = $data["birthday"];
    $alamat = $data["alamat"];
    $tanggal_kunjungan = $data["tglkunjungan"];
    $jenis_bayar =$data["jenisBayar"];
    $puskesmas = $data["puskesmas"];
    $poli = $data["poli"];

    // cek user yng sudah ada
    $result = mysqli_query($conn, "SELECT nik FROM pasien WHERE nik='$nik'");
        
    if (mysqli_fetch_assoc($result)){
        echo "<script>
                alert('NIK yang digunakan sudah terdaftar!')
            </script>";
        return false;
    }

    //menambah user baru ke db
    mysqli_query($conn, "INSERT INTO pasien VALUES('$nik', '$nama', '$kpl_keluarga', '$kelamin', '$tanggal_lahir', '$alamat')");
   if(mysqli_affected_rows($conn)>0){
    mysqli_query($conn, "INSERT INTO formulir VALUES('', '$tanggal_kunjungan', '$jenis_bayar', '$poli', '$puskesmas', '$nik')");
   }
    $form = query("SELECT * FROM formulir where nik='$nik'")[0];
    // var_dump($form);
    antrian($form);
    return mysqli_affected_rows($conn);

}

function pasienlama($data){
    global $conn;
    
    $nik= $data["NIK"];
    $tanggal_kunjungan = $data["tglkunjungan"];
    $jenis_bayar =$data["jenisBayar"];
    $puskesmas = $data["puskesmas"];
    $poli = $data["poli"];

    // cek user yng sudah ada
    $result = mysqli_query($conn, "SELECT * FROM pasien WHERE NIK='$nik'");
    if (!mysqli_fetch_assoc($result)){
        return false;
    }

     //menambah user baru ke db
     mysqli_query($conn, "INSERT INTO formulir VALUES('', '$tanggal_kunjungan', '$jenis_bayar', '$poli', '$puskesmas', '$nik')");
     $affrow = mysqli_affected_rows($conn);
     $form = query("SELECT * FROM formulir where NIK='$nik' order by kode_form DESC limit 1");
    //  var_dump($form[0]);
     antrian($form[0]);
     return $affrow;


}


function antrian($data){
    global $conn;
    // var_dump($data);
    $kdform = $data["kode_form"];
    $kodepoli = $data["kode_poli"];
    $kodepuskesmas = $data["kode_puskesmas"];
    $nomor_antrian = query("SELECT COUNT(kode_poli) FROM antrian INNER JOIN formulir on antrian.kode_form = formulir.kode_form WHERE kode_poli = '$kodepoli' and kode_puskesmas ='$kodepuskesmas'");
    
    // var_dump($nomor_antrian[0]["COUNT(kode_poli)"]);
    $nomor = $nomor_antrian[0]["COUNT(kode_poli)"]+1;
    $_SESSION["nomor"] = $nomor;
    mysqli_query($conn, "INSERT INTO antrian VALUES('$nomor', '$kdform', 'Dalam Antrian')");
    

}