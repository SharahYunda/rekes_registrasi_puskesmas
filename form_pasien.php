<?php
require 'function.php';


$kode_puskesmas = $_GET["pilihpuskesmas"];
$kode_poli = $_GET["poli"];


if (isset($_POST["masuk"])) {
    if(pasienlama($_POST)> 0){
        $nik = $_POST["NIK"];
        echo "<script>
                alert('Anda berhasil mengambil antrian');
                window.open('bookingcard.php?nik=$nik');
        </script>";
        // header("location: bookingcard.html");
    } else {
        echo "<script>
        alert('NIK Tidak Terdaftar!');
        window.location.href='form_pasien_baru.php?pilihpuskesmas=$kode_puskesmas&poli=$kode_poli';
        </script>";
    }
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FORM PASIEN LAMA</title>
    <link href='http:https://badoystudio.com/cloudme.fonts.googleapis.com/css?family=Bitter' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="reset.css">
    <link rel="stylesheet" href="Header.css">
    <link rel="stylesheet" href="form_pasien.css">

</head>

<body>
    <header>
        <div class="atas">
            <a href="HOME.html">
                <img src="icon/logo-puskesmas-terbaru-sesuai-permenkes-tahun-1.png" alt="foto puskesmas">
                <h1>REKES</h1>
            </a>


            <ul>
                <li> <a href="HOME.html"> REGISTRASI </a></li>
                <li> <a href="pencarian.php" target="blank"> PENCARIAN </a></li>
                <li> <a href="petunjuk.html"> PETUNJUK </a></li>
                <li> <a href="pengunjung.php"> ANTRIAN </a></li>
                <li> <a href="maps.html" target="blank"> MAPS </a></li>
            </ul>
        </div>
    </header>
    <main>
        <div class="form-style-10">
            <h1>FORM PASIEN LAMA<span>Mohon Semua data Diatas Diisi Untuk Mendapatkan No.Antrian</span></h1>
            <form action="" method="post">
                <div class="inner-wrap">
                    <label>NIK<input type="text" name="NIK" required /></label>
                    <label for="tglkunjungan">Tanggal Kunjungan</label>
                    <input type="date" id="tglkunjungan" name="tglkunjungan" required>
                    <label class="dropdown" for="jenisBayar">Jenis Bayar</label>
                    <select name="jenisBayar" id="jenisBayar">
                        <option value="1">Jenis Bayar</option>
                        <option value="Umum">Umum</option>
                        <option value="BPJS">BPJS</option>
                    </select>
                    <input type="hidden" name="puskesmas" value="<?= $kode_puskesmas; ?>">
                    <input type="hidden" name="poli" value="<?= $kode_poli; ?>"> 
                </div>
                <div class="button-section">
                    <button type="submit" name="masuk">DAFTAR</button>
                </div>
            </form>
        </div>
    </main>

</body>

</html>