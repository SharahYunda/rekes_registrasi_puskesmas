<?php 
require 'function.php';

if (isset($_POST["signup"])) {
    if(registrasi($_POST)> 0){
        // echo "test";
        $nik = $_POST["NIK"];
        echo " <script> 
        alert('Anda Berhasil Mendapatkan No. Antrian!');
            window.open('bookingcard.php?nik=$nik');
        </script>";  
    }
}

$kode_puskesmas = $_GET["pilihpuskesmas"];
$kode_poli = $_GET["poli"];



?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FORM PASIEN BARU</title>
    <link href='http:https://badoystudio.com/cloudme.fonts.googleapis.com/css?family=Bitter' rel='stylesheet'
        type='text/css'>
    <link rel="stylesheet" href="reset.css">
    <link rel="stylesheet" href="Header.css">
    <link rel="stylesheet" href="form_pasien.css">
    <link rel="stylesheet" href="footer.css">

</head>

<body>
    <header>
        <div class="atas">
            <a href="HOME.html">
                <img src="icon/logo-puskesmas-terbaru-sesuai-permenkes-tahun-1.png" alt="foto puskesmas">
                <h1>REKES</h1>
            </a>
            

            <ul>
                <li> <a href="HOME.html"> REGISTRASI </a></li>
                <li> <a href="pencarian.php" target="blank"> PENCARIAN </a></li>
                <li> <a href="petunjuk.html"> PETUNJUK </a></li>
                <li> <a href="pengunjung.php"> ANTRIAN </a></li>
                <li> <a href="maps.html" target="blank"> MAPS </a></li>
            </ul>
        </div>

    </header>
    <main>
        <div class="form-style-10">
            <h1>FORM PASIEN BARU<span>Mohon Semua data Diatas Diisi Untuk Mendapatkan No.Antrian</span></h1>
            <form action="" method="post">
                <div class="inner-wrap">
                    <label>NIK<input type="text" name="NIK" required/></label>
                    <label>Nama<input type="text" name="Nama" required/></label>
                    <label>Nama Kepala Kelaurga<input type="text" name="kpl_keluarga" required/></label>
                    <label for="status">Jenis Kelamin : </label>
                    <input type="radio" id="siswa" name="status" value="Pria" required>
                    <label for="siswa">Pria</label>
                    <input type="radio" id="guru" name="status" value="Wanita"required>
                    <label for="guru">Wanita</label>
                    <label for="birthday">Tanggal Lahir</label>
                    <input type="date" id="birthday" name="birthday" required>
                    <label>Alamat<textarea name="alamat" required></textarea></label>
                    <label for="tglkunjungan">Tanggal Kunjungan</label>
                    <input type="date" id="tglkunjungan" name="tglkunjungan" required>
                    <label class="dropdown" for="jenisBayar">Jenis Bayar</label>
                    <select name="jenisBayar" id="jenisBayar">
                        <option>Jenis Bayar</option>
                        <option value="Umum">Umum</option>
                        <option value="BPJS">BPJS</option>
                    </select>
                    <input type="hidden" name="puskesmas" value="<?= $kode_puskesmas; ?>">
                    <input type="hidden" name="poli" value="<?= $kode_poli; ?>"> 
                </div>
                <div class="button-section">
                    <button type="submit" name="signup">DAFTAR</button>
                </div>
            </form>
        </div>
    </main>
    <footer>
        <div class="containerFooter">
            <div class="rekes">
                <h2>REKES</h2>
                <p>&copy;2020 UwU Dev, Inc</p>
                <br><br>
                <p>Mataram, <br>Lombok Indonesia</p>
            </div>
            <div class="tentang">
                <ul>
                    <a href="">
                        <li>HOME</li>
                    </a>
                    <li>OPEN <br> 08.00 WITA </li>
                </ul>
            </div>
            <div class="contact">
                <ul>
                    <a href="">
                        <li>CONTACT</li>
                    </a>
                    <a href="">
                        <li>Term of Use</li>
                    </a>
                    <a href="">
                        <li>Privacy Policy</li>
                    </a>
                    </a>
                </ul>
            </div>
            <div class="faq">
                <ul>
                    <a href="faq.html">
                        <li>FAQ</li>
                    </a>
                        <li> 
                        <a href="">
                         <img src="icon/youtube.png" alt="">
                        </a>
                           <a href="">
                            <img src="icon/facebook-logo.png" alt="">
                           </a>
                            
                        </li>
                   
                </ul>
            </div>
        </div>
    </footer>

</body>
</html>