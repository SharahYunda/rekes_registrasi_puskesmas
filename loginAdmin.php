<?php 
require 'function.php';

if(isset($_SESSION["login"])) {
	echo "<script> window.location.href='admin.php'; </script>";
}

if(isset($_POST["login"])){
  $username = $_POST["username"];
  // session_start();
  $_SESSION["uname"]  = $_POST["username"];
  $password = $_POST["password"];


  $result = mysqli_query($conn, "SELECT * FROM admin WHERE username='$username'");
  
  if(  mysqli_num_rows($result) === 1){ 
     $row =  mysqli_fetch_assoc($result);
     
     if($password === $row["password"]){
        $_SESSION["login"] = true;
        $_SESSION["id"]  = $row["id_admin"];
        $_SESSION["puskesmas"] = $row["kode_puskesmas"];
         header("Location: admin.php");
        
         exit;
     }
  

  }
  
  $error=true;
  
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="reset.css">
    <link rel="stylesheet" href="loginAdmin.css">
   
</head>
<body>
  <header>
        <a href="#menu">
            <div class="atas">
                <img src="icon/logo-puskesmas-terbaru-sesuai-permenkes-tahun-1.png" alt="foto puskesmas">
                <h1>REKES</h1>
            </div>
        </a>
    </header>
    <div class="login-box">
        <h2>LOGIN</h2>
        <?php if( isset($error)) : ?>
          <p> username/pass salah</p>
        <?php endif; ?>

        <form action="" method="post">
          <div class="user-box">
            <input type="text" name="username" required="username">
            <label>Username</label>
          </div>
          <div class="user-box">
            <input type="password" name="password" required="password">
            <label>Password</label>
          </div>
          <div class="a">
            <span></span>
            
            <button type= "submit" name="login"> <span></span>
            <span></span>
            <span></span> LOGIN</button>
          </div>
        </form>
      </div>
</body>
</html>

