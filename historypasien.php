<?php 

require 'function.php';
$dataNik = $_GET["history"];
$dataform = query("SELECT formulir.kode_form, formulir.tanggal_kunjungan, poli.nama_poli, formulir.jenis_bayar from formulir inner join poli on formulir.kode_poli = poli.kode_poli WHERE nik = $dataNik");

$namaPasien = query("SELECT Nama from pasien where nik=$dataNik");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HISTORY PASIEN</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@600&family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="reset.css">
    <link rel="stylesheet" href="Header.css">
    <link rel="stylesheet" href="historypasien.css">

</head>

<body>
    <header>
        <div class="atas">
            <img src="icon/logo-puskesmas-terbaru-sesuai-permenkes-tahun-1.png" alt="foto puskesmas">
            <h1>REKES</h1>

            <ul>
                <li> <a href="datapasien.php" target="blank"> DATA PASIEN </a></li>
                <li> <a href="admin.php"> ANTRIAN </a></li>
                <li> <a href="" target="blank"> PROFILE </a></li>
                <li> <a href="logout.php"> LOGOUT </a></li>
            </ul>
        </div>
    </header>

    <main>

        <div class="judul">
            <h1><?= $namaPasien[0]["Nama"]  ?></h1>
            <h2><?= $dataNik  ?></h2>
        </div>
        
        <table style="margin-top:30px;">
            <tr>
                <th>KODE FORM</th>
                <th>TANGGAL KUNJUNGAN</th>
                <th>POLI</th>
                <th>JENIS_BAYAR</th>
            </tr>
            <?php foreach($dataform as $data):  ?>
            <tr>
                <th><?= $data["kode_form"]  ?></th>
                <th><?= $data["tanggal_kunjungan"]  ?></th>
                <th><?= $data["nama_poli"]  ?></th>
                <th><?= $data["jenis_bayar"]  ?></th>
            </tr>
            <?php endforeach;  ?>
            
        </table>
    </main>
</body>
<script src="test.js"></script>

</html>