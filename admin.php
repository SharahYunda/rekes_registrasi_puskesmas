<?php
function query($query)
{

	global $conn;
	$result = mysqli_query($conn, $query);
	$rows = [];
	while ($row = mysqli_fetch_assoc($result)) {
		$rows[] = $row;
	}
	return $rows;
}
session_start();
if (!isset($_SESSION["login"])) {
	echo "<script> window.location.href='loginAdmin.php'; </script>";
	exit;
}

$conn = mysqli_connect("localhost", "root", "", "registrasi_puskesmas");
$kecamatan = query("SELECT * FROM kecamatan");
// $admin = $_SESSION["uname"];
// $puskesmas = query("SELECT kode_puskesmas from admin where username=$admin")
// var_dump($kecamatan);
$puskesmas = $_SESSION["puskesmas"];
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>ANTRIAN PASIEN</title>
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@600&family=Roboto&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="reset.css">
	<link rel="stylesheet" href="Header.css">
	<link rel="stylesheet" href="admin.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
	<header>
		<div class="atas">
			<img src="icon/logo-puskesmas-terbaru-sesuai-permenkes-tahun-1.png" alt="foto puskesmas">
			<h1>REKES</h1>

			<ul>
				<li> <a href="datapasien.php"> DATA PASIEN </a></li>
				<li> <a href="admin.php"> ANTRIAN </a></li>
				<li> <a href="profile.php"> PROFILE </a></li>
				<li> <a href="logout.php"> LOGOUT </a></li>
			</ul>
		</div>
	</header>
	
	
	<main>
		<div class="judul">
			<h1>ANTRIAN</h1>
		</div>

		<!-- <form action="admin.php" method="get">

			<input type="text" name="cari" class="input">
			<input type="submit" value="Search" class="button">
		</form> -->
		<br>
		<table style="margin-top:30px;">
			<tr>
				<th>No.</th>
				<th>NIK/No.Simpus</th>
				<th>Nama</th>
				<th>Tgl Kunjungan</th>
				<th>Poli</th>
				<th>Puskesmas</th>
				<th>Antrian</th>
				<th>Status</th>
			</tr>
			
				<?php
				$con = mysqli_connect("localhost", "root", "", "registrasi_puskesmas");
				$username = $_SESSION["uname"];
				if (!$con) {
					die("Connection failed: " . mysqli_connect_error());
				}
				// $pusadm = "SELECT kode_puskesmas from admin where email = 	$mail;
				$sql = "SELECT formulir.nik, pasien.nama, formulir.tanggal_kunjungan, poli.nama_poli, puskesmas.nama_puskesmas, antrian.no_antrian, antrian.kode_form, antrian.status from pasien inner join formulir on pasien.nik = formulir.nik inner join poli on formulir.kode_poli = poli.kode_poli inner join puskesmas on formulir.kode_puskesmas = puskesmas.kode_puskesmas inner join antrian on formulir.kode_form = antrian.kode_form where puskesmas.kode_puskesmas in (SELECT kode_puskesmas from admin where username='$username')";

				$result = mysqli_query($con, $sql);
				$i = 1;
				if (mysqli_num_rows($result)) {
					while ($row = mysqli_fetch_array($result)):?>
						<tr>
							<td><?=$i++ ?></td>
							<td><?=$row[0] ?></td>
							<td><?=$row[1] ?></td>
							<td><?=$row[2] ?></td>
							<td><?=$row[3] ?></td>
							<td><?=$row[4] ?></td>
							<td><?=$row[5] ?></td>
							<td>
								<a href="updateStatus.php?kodeform=<?= $row[6]  ?>">
									<input class type='submit' id='ubhstatus' value="<?= $row[7]  ?>">
								</a>
							</td>
						</tr>
					<?php endwhile;  
					
				}
				?>
		</table>

		<!-- <?php
		echo "<input class='buttonDel' type='submit' id='delete' value='Hapus Antrian'>";
		?> -->
		<div class="hapus">
			<a href="hapusAntrian.php?puskesmas=<?= $puskesmas?>" onclick="return confirm('yakin?');">hapus</a>
		</div>
		
	</main>
</body>
<script src="test.js"></script>

</html>