<?php
require 'function.php';
// ambil data artikel dari database
// $none="Pencarian ditemukan";
$artikels = null;
if(isset($_GET["search"])) {
    $nik = $_GET["search"];
    $artikels = query("SELECT formulir.nik, pasien.nama, formulir.tanggal_kunjungan, poli.nama_poli, puskesmas.nama_puskesmas, antrian.no_antrian from pasien inner join formulir on pasien.nik = formulir.nik inner join poli on formulir.kode_poli = poli.kode_poli inner join puskesmas on formulir.kode_puskesmas = puskesmas.kode_puskesmas inner join antrian on formulir.kode_form = antrian.kode_form WHERE formulir.nik = '$nik'");
}



?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Pencarian</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="reset.css">
    <link rel="stylesheet" href="Header.css">
    <link rel="stylesheet" href="css/dist/css/bootstrap.css">
    <link rel="stylesheet" href="pencarian.css">
</head>

<body>
    <header>

        <div class="atas">
            <a href="HOME.html">
                <img src="icon/logo-puskesmas-terbaru-sesuai-permenkes-tahun-1.png" alt="foto puskesmas">
                <h1>REKES</h1>
            </a>
            <ul>
                <li> <a href="kecamatan.php"> REGISTRASI </a></li>
                <li> <a href="pencarian.php" target="blank"> PENCARIAN </a></li>
                <li> <a href="petunjuk.html"> PETUNJUK </a></li>
                <li> <a href="pengunjung.php"> ANTRIAN </a></li>
                <li> <a href="maps.html" target="blank"> MAPS </a></li>
            </ul>
        </div>
    </header>
    <main>
        <h1> PENCARIAN DATA PASIEN </h1>
        <nav class="navbar navbar-light ">

            <form action="pencarian.php" class="form-inline" method="GET">
                <input class="form-control mr-sm-10" placeholder="Search" aria-label="Search" name="search" required>
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </nav>
        <?php 
        if($artikels == null):?>
            <p style="text-align: center;margin-top: 40px;">Tidak ada pencarian</p>
        <?php else:?>
        
        <table style="margin-top:30px;">
            <tr>
                
                <th>NIK/No.Simpus</th>
                <th>Nama</th>
                <th>Tgl Kunjungan</th>
                <th>Poli</th>
                <th>Puskesmas</th>
                <th>Antrian</th>
                
            </tr>
            <?php foreach($artikels as $artikel):  ?>
            
            <tr>
                <th><?= $artikel["nik"] ?></th>
                <th><?= $artikel["nama"] ?></th>
                <th><?= $artikel["tanggal_kunjungan"] ?></th>
                <th><?= $artikel["nama_poli"] ?></th>
                <th><?= $artikel["nama_puskesmas"] ?></th>
                <th><?= $artikel["no_antrian"] ?></th>
            </tr>
            <?php endforeach;  ?>   
        </table>
        <?php endif;  ?>
        
        </div>
    </main>

    <footer>
        <div class="containerFooter">
            <div class="rekes">
                <h2>REKES</h2>
                <p>&copy;2020 UwU Dev, Inc</p>
                <!-- <br><br>
            <p>Mataram, <br>Lombok Indonesia</p> -->
            </div>
            <div class="tentang">
                <ul>
                    <a href="">
                        <li>HOME</li>
                    </a>
                    <li>OPEN <br> 08.00 WITA </li>
                </ul>
            </div>
            <div class="contact">
                <ul>
                    <a href="">
                        <li>CONTACT</li>
                    </a>
                    <a href="">
                        <li>Term of Use</li>
                    </a>
                    <a href="">
                        <li>Privacy Policy</li>
                    </a>
                    </a>
                </ul>
            </div>
            <div class="faq">
                <ul>
                    <a href="faq.html">
                        <li>FAQ</li>
                    </a>
                    <li>
                        <a href="">
                            <img src="icon/youtube.png" alt="">
                        </a>
                        <a href="">
                            <img src="icon/facebook-logo.png" alt="">
                        </a>

                    </li>

                </ul>
            </div>
        </div>
    </footer>
</body>

</html>