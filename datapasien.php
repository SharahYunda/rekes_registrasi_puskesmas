<?php 
require 'function.php';

$username = $_SESSION["uname"];
$datapasien = query ("SELECT * FROM pasien where nik IN(SELECT nik from formulir where kode_puskesmas in (SELECT kode_puskesmas from admin where username='$username'))");
 


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DATA PASIEN</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@600&family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="reset.css">
    <link rel="stylesheet" href="Header.css">
    <link rel="stylesheet" href="datapasien.css">

</head>

<body>
    <header>
        <div class="atas">
            <img src="icon/logo-puskesmas-terbaru-sesuai-permenkes-tahun-1.png" alt="foto puskesmas">
            <h1>REKES</h1>

            <ul>
                <li> <a href="datapasien.php" target="blank"> DATA PASIEN </a></li>
                <li> <a href="admin.php"> ANTRIAN </a></li>
                <li> <a href="" target="blank"> PROFILE </a></li>
                <li> <a href="logout.php"> LOGOUT </a></li>
            </ul>
        </div>
    </header>

    <main>

        <div class="judul">
            <h1> DATA PASIEN </h1>
        </div>
        <!-- <form action="admin.php" method="get">
            <label>Search :</label>
            <input type="text" name="cari" class="input">
            <input type="submit" value="Search" class="button">
        </form> -->
        <br>
        <table style="margin-top:30px;">
            <tr>
                <th>No.</th>
                <th>NIK</th>
                <th>NAMA</th>
                <th>KEPALA KELUARGA</th>
                <th>JENIS KELAMIN</th>
                <th>TANGGAL LAHIR</th>
                <th>ALAMAT</th>
                <th>HISTORY</th>
                
            </tr>
            
            <?php   
                $i=1;
            foreach($datapasien as $dtpasien): ?>
                <tr>
                    <th><?= $i++  ?></th>
                    <th><?= $dtpasien["NIK"]  ?></th>
                    <th><?= $dtpasien["Nama"]  ?></th>
                    <th><?= $dtpasien["Nama_kpl_keluarga"]  ?></th>
                    <th><?= $dtpasien["jenis_kelamin"]  ?></th>
                    <th><?= $dtpasien["tanggal_lahir"]  ?></th>
                    <th><?= $dtpasien["alamat"]  ?>
                    </th>
                    <th><a href="historypasien.php?history= <?= $dtpasien["NIK"]  ?> ">CEK HISTORY</a></th>    
                </tr>
            <?php endforeach;  ?>
        </table>
    </main>
</body>
<script src="test.js"></script>

</html>