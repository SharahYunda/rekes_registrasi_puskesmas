<?php
function query($query)
{
    global $conn;
    $result = mysqli_query($conn, $query);
    $rows = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }
    return $rows;
}
session_start();

$conn = mysqli_connect("localhost", "root", "", "registrasi_puskesmas");
$kecamatan = query("SELECT * FROM kecamatan");
// var_dump($kecamatan);


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@600&family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="reset.css">
    <link rel="stylesheet" href="Header.css">
    <link rel="stylesheet" href="kecamatan.css">
    <link rel="stylesheet" href="footer.css">
	<style>
		table, td, th {  
		  border: 1px solid #ddd;
		  text-align: left;
		}

		table {
		  border-collapse: collapse;
		  width: 100%;
		}

		th, td {
		  padding: 15px;
		}
		.button {
		  background-color: #4CAF50;
		  border: none;
		  color: white;
		  height: 30px;
		  width : 70px;
		  text-align: center;
		  text-decoration: none;
		  display: inline-block;
		  font-size: 16px;
		  margin: 4px 2px;
		  cursor: pointer;
		  border-radius:5px;
		}
		.input {
			height:20px;
		}
		.buttonDel{
		  background-color: red;
		  border: none;
		  color: white;
		  height: 30px;
		  width : 70px;
		  text-align: center;
		  text-decoration: none;
		  display: inline-block;
		  font-size: 16px;
		  margin: 4px 2px;
		  cursor: pointer;
		  border-radius:5px;
		}
	</style>
</head>

<body>
    <header>
        <div class="atas">
            <a href="HOME.html">
                <img src="icon/logo-puskesmas-terbaru-sesuai-permenkes-tahun-1.png" alt="foto puskesmas">
                <h1>REKES</h1>
            </a>

            <ul>
                <li> <a href="HOME.html"> REGISTRASI </a></li>
                <li> <a href="pencarian.php" target="blank"> PENCARIAN </a></li>
                <li> <a href="petunjuk.html"> PETUNJUK </a></li>
                <li> <a href="pengunjung.php"> ANTRIAN </a></li>
                <li> <a href="maps.html" target="blank"> MAPS </a></li>
            </ul>
        </div>
    </header>
	
    <main>
		
		<p style="font-size: 25px;" >Daftar Antrian Pasien</p>
		<!-- <form action="admin.php" method="get">
			<label>Search :</label>
			<input type="text" name="cari" class="input">
			<input type="submit" value="Search" class="button">
		</form> -->
		<!-- <br> -->
		<table style="margin-top:30px;">
		  <tr>
			<th>No</th>
			<th>NIK/No.Simpus</th>
			<th>Nama</th>
			<th>Nama Kk</th>
			<th>Tgl Kunjungan</th>
			<th>Puskesmas</th>
			<th>Antrian</th>
			<!-- <th>Action</th> -->
		  </tr>
			<form name="actionButton" action="admin.php" method="get">
			<?php
				$con = mysqli_connect("localhost","root","","registrasi_puskesmas");
				if(!$con){
					die("Connection failed: " . mysqli_connect_error());
				}
				
				$sql = "SELECT p.NIK,p.Nama,p.Nama_kpl_keluarga,f.tanggal_kunjungan,a.no_antrian, pu.nama_puskesmas FROM pasien AS p INNER JOIN formulir AS f ON p.NIK = f.NIK INNER JOIN antrian AS a ON f.kode_form = a.kode_form INNER JOIN puskesmas AS pu ON f.kode_puskesmas = pu.kode_puskesmas";
				
				$result = mysqli_query($con, $sql);
				$i = 1;
				if(mysqli_num_rows($result)){
					while($row = mysqli_fetch_array($result)){
						echo "<tr><td>".$i++."</td><td>".$row[0]."</td><td>".$row[1]."</td><td>".$row[2]."</td><td>".$row[3]."</td><td>".$row[5]."</td><td>".$row[4]."</td>";
						// echo"<td><input class='buttonDel' type='submit' id='delete' value='Delete'></td></tr>";
					}
				}
			?>
			</form>
		</table>
    </main>
    <footer>
        <div class="containerFooter">
            <div class="rekes">
                <h2>REKES</h2>
                <p>&copy;2020 UwU Dev, Inc</p>
                <br><br>
                <p>Mataram, <br>Lombok Indonesia</p>
            </div>
            <div class="tentang">
                <ul>
                    <a href="">
                        <li>HOME</li>
                    </a>
                    <li>OPEN <br> 08.00 WITA </li>
                </ul>
            </div>
            <div class="contact">
                <ul>
                    <a href="">
                        <li>CONTACT</li>
                    </a>
                    <a href="">
                        <li>Term of Use</li>
                    </a>
                    <a href="">
                        <li>Privacy Policy</li>
                    </a>
                    </a>
                </ul>
            </div>
            <div class="faq">
                <ul>
                    <a href="faq.html">
                        <li>FAQ</li>
                    </a>
                        <li> 
                        <a href="">
                         <img src="icon/youtube.png" alt="">
                        </a>
                           <a href="">
                            <img src="icon/facebook-logo.png" alt="">
                           </a>
                            
                        </li>
                   
                </ul>
            </div>
        </div>
    </footer>
</body>
<script src="test.js"></script>

</html>