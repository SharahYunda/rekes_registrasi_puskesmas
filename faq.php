<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FAQ</title>
    <link rel="stylesheet" href="reset.css">
    <link rel="stylesheet" href="Header.css">
    <link rel="stylesheet" href="faq.css">
    <link rel="stylesheet" href="footer.css">   
</head>
<body>
    <header>
        <div class="atas">
            <a href="HOME.html">
                <img src="icon/logo-puskesmas-terbaru-sesuai-permenkes-tahun-1.png" alt="foto puskesmas">
                <h1>REKES</h1>
            </a>
        <ul>
            <li> <a href="HOME.html"> REGISTRASI </a></li>
            <li> <a href="pencarian.html" target="blank"> PENCARIAN </a></li>
            <li> <a href=""> PETUNJUK </a></li>
            <li> <a href=""> ANTRIAN </a></li>
            <li> <a href="maps.html" target="blank"> MAPS </a></li>
        </ul>
    </div>

</header>
<main>
    <div class="judul">
        <h2>Frequently Asked Questions</h2>
    </div>
    <div style="visibility: hidden; position: absolute; width: 0px; height: 0px;">
      <svg xmlns="http://www.w3.org/2000/svg">
        <symbol viewBox="0 0 24 24" id="expand-more">
          <path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z"/><path d="M0 0h24v24H0z" fill="none"/>
        </symbol>
        <symbol viewBox="0 0 24 24" id="close">
          <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/><path d="M0 0h24v24H0z" fill="none"/>
        </symbol>
      </svg>
    </div>
    
    <details open>
      <summary>
        Apa itu REKES?
        <svg class="control-icon control-icon-expand" width="24" height="24" role="presentation"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#expand-more" /></svg>
        <svg class="control-icon control-icon-close" width="24" height="24" role="presentation"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#close" /></svg>
      </summary>
      <p>REKES merupakan sistem informasi yang digunakan untuk melakukan pendaftaran pada seluruh puskesmas yang ada di kota Mataram secara online.</p>
    </details>
    
    <details>
      <summary>
        Bisakah REKES digunakan setiap saat?
        <svg class="control-icon control-icon-expand" width="24" height="24" role="presentation"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#expand-more" /></svg>
        <svg class="control-icon control-icon-close" width="24" height="24" role="presentation"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#close" /></svg>
      </summary>
      <p>Tentu saja REKES dapat digunakan setiap saat, dimana saja dan kapan saja.</p>
    </details>
    
    <details>
      <summary>
        Puskesmas mana saja yang dapat digunakan untuk mendaftar di REKES?    
        <svg class="control-icon control-icon-expand" width="24" height="24" role="presentation"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#expand-more" /></svg>
        <svg class="control-icon control-icon-close" width="24" height="24" role="presentation"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#close" /></svg>
      </summary>
      <p>Only your imagination my friend. Go forth!</p>
    </details>
</main>
<footer>
    <div class="containerFooter">
        <div class="rekes">
            <h2>REKES</h2>
            <p>&copy;2020 UwU Dev, Inc</p>
            <br><br>
            <p>Mataram, <br>Lombok Indonesia</p>
        </div>
        <div class="tentang">
            <ul>
                <a href="">
                    <li>HOME</li>
                </a>
                <li>OPEN <br> 08.00 WITA </li>
            </ul>
        </div>
        <div class="contact">
            <ul>
                <a href="">
                    <li>CONTACT</li>
                </a>
                <a href="">
                    <li>Term of Use</li>
                </a>
                <a href="">
                    <li>Privacy Policy</li>
                </a>
                </a>
            </ul>
        </div>
        <div class="faq">
            <ul>
                <a href="faq.html">
                    <li>FAQ</li>
                </a>
                    <li> 
                        <a href="">
                            <img src="icon/youtube.png" alt="">
                        </a>
                        <a href="">
                            <img src="icon/facebook-logo.png" alt="">
                        </a>
                    </li>
            </ul>
        </div>
    </div>
</footer>
</body>
</html>