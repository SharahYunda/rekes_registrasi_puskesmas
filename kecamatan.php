<?php
function query($query){
    global $conn;
    $result = mysqli_query($conn, $query);
    $rows = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }
    return $rows;
}
session_start();

$conn = mysqli_connect("localhost", "root", "", "registrasi_puskesmas");
$kecamatan = query("SELECT * FROM kecamatan");
// var_dump($kecamatan);


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Memilih Kecamatan</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@600&family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="reset.css">
    <link rel="stylesheet" href="Header.css">
    <link rel="stylesheet" href="kecamatan.css">
    <link rel="stylesheet" href="footer.css">
    
</head>

<body>
    <header>

        <div class="atas">
            <a href="HOME.html">
                <img src="icon/logo-puskesmas-terbaru-sesuai-permenkes-tahun-1.png" alt="foto puskesmas">
                <h1>REKES</h1>
            </a>

            <ul>
                <li> <a href="HOME.html"> REGISTRASI </a></li>
                <li> <a href="pencarian.php" target="blank"> PENCARIAN </a></li>
                <li> <a href="petunjuk.html"> PETUNJUK </a></li>
                <li> <a href="pengunjung.php"> ANTRIAN </a></li>
                <li> <a href="maps.html" target="blank"> MAPS </a></li>
            </ul>
        </div>
        
    </header>

    <main>
        <div class="judul">
            <h1>PILIH KECAMATAN</h1>
        </div> 
        <?php

        // $kecamatan = array("mataram", "jawa","selong","narmada");
        
        foreach ($kecamatan as $kec) :  ?>
        
            <a href="puskesmas.php?pilihkecamatan=<?php echo $kec["kode_kecamatan"] ?>">
                <div class="bungkusMenu">
                    <div class="menu">
                        <img src="icon/map (3).png" alt="puskesmas.php">
                    </div>
                    <p>
                        <?php echo  strtoupper($kec["nama_kecamatan"])  ?>
                    </p>
                </div>
            </a>
        <?php endforeach;  ?>
    </main>


    <footer>
        <div class="containerFooter">
            <div class="rekes">
                <h2>REKES</h2>
                <p>&copy;2020 UwU Dev, Inc</p>
                <br><br>
                <p>Mataram, <br>Lombok Indonesia</p>
            </div>
            <div class="tentang">
                <ul>
                    <a href="">
                        <li>HOME</li>
                    </a>
                    <li>OPEN <br> 08.00 WITA </li>
                </ul>
            </div>
            <div class="contact">
                <ul>
                    <a href="">
                        <li>CONTACT</li>
                    </a>
                    <a href="">
                        <li>Term of Use</li>
                    </a>
                    <a href="">
                        <li>Privacy Policy</li>
                    </a>
                    </a>
                </ul>
            </div>
            <div class="faq">
                <ul>
                    <a href="faq.php">
                        <li>FAQ</li>
                    </a>
                        <li> 
                        <a href="">
                         <img src="icon/youtube.png" alt="">
                        </a>
                           <a href="">
                            <img src="icon/facebook-logo.png" alt="">
                           </a>
                            
                        </li>
                </ul>
            </div>
        </div>
    </footer>


</body>
<!-- <script src="test.js"></script> -->

</html>